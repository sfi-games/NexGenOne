using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// The following are added
using UnityEngine.UI;
using TMPro;


public class RecoverRobotParts : MonoBehaviour
{
    public int numberOfParts;
    public TextMeshProUGUI missionText;
    public GameObject missionbuttom;
    void Start()
    {
        numberOfParts = GameObject.FindGameObjectsWithTag("Objective").Length;
        missionText.text = "Recover the hidden parts of the robot " +
            "\n Remaining: " + numberOfParts;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Objective")
        {
            Destroy(col.transform.parent.gameObject);
            numberOfParts--;
            missionText.text = "Recover the hidden parts of the robot " +
            "\n Remaining: " + numberOfParts;
            if (numberOfParts <= 0)
            {
                missionText.text = "You completed the mission";
                missionbuttom.SetActive(true);
            }
        }
    }
}